import { ITodo } from "../types/todos.interfaces";

export interface CreateTodoDto extends ITodo {
    title: string;
    description: string;
}

export interface putTodoDto extends ITodo {
    id: number;
    title: string;
    description: string;
}