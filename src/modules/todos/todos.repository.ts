import { CRUD } from "../../common/interfaces/crud.interface";
import {Knex} from 'knex';
import { Todo } from "./entities/todo.entity";
import { ITodo } from "./types/todos.interfaces";

class TodoRepository implements CRUD<ITodo>{
    
    constructor(private readonly connection: Knex) {}

    findAll = () => {
        return this.connection('Todos').where<Todo[]>('isDeleted', false);
    }
    findOne = async (id: number) => {
        return await this.connection('Todos').where<Todo>('id', id).where<Todo>('isDeleted', false);
    }
    updateOne = (id: number, todo: ITodo) => {
        return this.connection('Todos').where<Todo>('id', id).update<Todo>(todo);
    }
    create = (todo: ITodo) => {
        return this.connection('Todos').insert<Todo>({...todo, isDeleted: false});
    }
    deleteOne = (id: number) => {
        return this.connection('Todos').where<Todo>('id', id).update<Todo>({isDeleted: true});
    } 
}

export default TodoRepository;