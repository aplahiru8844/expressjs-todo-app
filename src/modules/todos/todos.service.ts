import { database } from '../../configs/db.config';
import { CreateTodoDto, putTodoDto } from './dtos/todos.dtos';
import { Todo } from './entities/todo.entity';
import TodoRepository from './todos.repository';

class TodoService {
    constructor(private readonly todosRepository: TodoRepository) {}

    async create(todo: CreateTodoDto) {
        const todoObj = new Todo();
        todoObj.title = todo.title;
        todoObj.description = todo.description;
        return await this.todosRepository.create(todoObj);
    }
    async putById(id: number, todo: putTodoDto) {
        const todoObj = new Todo();
        todoObj.title = todo.title;
        todoObj.description = todo.description;
        todoObj.isDeleted = todo.isDeleted;
        return await this.todosRepository.updateOne(id, todoObj);
    }
    async list() {
        return await this.todosRepository.findAll();
    }
    async readById(id: number) {
        return await this.todosRepository.findOne(id);
    }
    async deleteById(id: number) {
        return await this.todosRepository.deleteOne(id);
    }
}

export default new TodoService(new TodoRepository(database));