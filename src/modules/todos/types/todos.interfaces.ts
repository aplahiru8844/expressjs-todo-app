export interface ITodo {
    title: string;
    description: string;
    isDeleted?: boolean;
    id?: number;
  }