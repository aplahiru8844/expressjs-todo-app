import express from 'express';
import todosService from './todos.service';

class TodoController {
    async listTodos(req: express.Request, res: express.Response) {
        try {
            const todos = await todosService.list();
            res.status(200).send(todos);
        }
        catch (error) {
            res.status(500).send(error);
        }
    }

    async getTodoById(req: express.Request, res: express.Response) {
        try {
            const todo = await todosService.readById(parseInt(req.params.todoId));
            res.status(200).send(todo);
        }
        catch (error) {
            res.status(500).send(error);
        }
    }

    async updateTodo(req: express.Request, res: express.Response) {
        try {
            const todo = await todosService.putById(parseInt(req.params.todoId), req.body);
            res.sendStatus(200).send(todo);
        }
        catch (error) {
            res.sendStatus(500).send(error);
        }
    }

    async deleteTodoById(req: express.Request, res: express.Response) {
        try {
            const todo = await todosService.deleteById(parseInt(req.params.todoId));
            res.sendStatus(200).send(todo);
        }
        catch (error) {
            res.sendStatus(500).send(error);
        }
    }

    async createTodo(req: express.Request, res: express.Response) {
        try {
            const todo = await todosService.create(req.body);
            res.status(200).send(todo);
        }
        catch (error) {
            res.status(500).send(error);
        }
    }
}

export default new TodoController();