import { ITodo } from "../types/todos.interfaces";

export class Todo implements ITodo {
    title!: string;
    description!: string;
    isDeleted?: boolean;
    id?: number;
}