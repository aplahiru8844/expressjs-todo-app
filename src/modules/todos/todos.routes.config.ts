import { CommonRoutesConfig } from '../../common/common.routes.config';
import express from 'express';
import todosController from './todos.controller';

export class TodosRoutes extends CommonRoutesConfig {
    constructor(app: express.Application) {
        super(app, 'TodosRoutes');
    }

    configureRoutes() {
        this.app
            .route(`/todos`)
            .get(todosController.listTodos)
            .post(todosController.createTodo);

        this.app
            .route(`/todos/:todoId`)
            .get(todosController.getTodoById)
            .delete(todosController.deleteTodoById)
            .put(todosController.updateTodo);

        return this.app;
    }
}