import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  const exists = await knex.schema.hasTable('todos')
    if (!exists) {
      return await knex.schema
        .createTable('todos', function (table) {
          table.increments('id').notNullable().primary();
          table.string('title').notNullable();
          table.string('description');
          table.integer('isDeleted').notNullable();
      });
    }
}


export async function down(knex: Knex): Promise<void> {
    return await knex.schema
      .dropTableIfExists('todos');
}

