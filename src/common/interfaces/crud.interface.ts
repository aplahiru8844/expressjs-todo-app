import { CreateTodoDto, putTodoDto } from "../../modules/todos/dtos/todos.dtos";

export interface CRUD<T> {
    findAll: () => Promise<T[]>;
    create: (todo: T) => Promise<any>;
    updateOne: (id: number, todo: T) => Promise<any>;
    findOne: (id: number) => Promise<T>;
    deleteOne: (id: number) => Promise<any>;
}