import express from 'express';
import cors from 'cors';
import { CommonRoutesConfig } from './common/common.routes.config';
import { TodosRoutes } from './modules/todos/todos.routes.config';

const app = express();

const port = 3000;

const routes: Array<CommonRoutesConfig> = [];

app.use(express.json());

app.use(cors());

app.use(express.urlencoded({extended: true}));

routes.push(new TodosRoutes(app));

app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});

// test route
app.get("/", (req, res) => {
    res.json({ message: "ok" });
});
